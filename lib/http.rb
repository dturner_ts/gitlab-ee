module JsonHTTPWrapper
  # Imitate HTTParty, which allows HTTP responses to be treated as JSON objects
  class << self
    include HTTPI

    def get(*args)
      wrap(HTTPI.get(*args))
    end

    def post(*args)
      wrap(HTTPI.post(*args))
    end

    private

    def wrap(response)
      JsonWrappedResponse.new(response)
    end
  end

  class Request < HTTPI::Request
  end
end

class JsonWrappedResponse
  def initialize(response)
    @response = response
  end

  def [](key)
    json[key]
  end

  def values
    json.values
  end

  def method_missing(method, *args)
    @response.send(method, *args)
  end

  def success?
    @response.code == 200
  end

  private

  def json
    @json ||= JSON.parse(@response.body)
  end

  def parsed_response
    json
  end
end
