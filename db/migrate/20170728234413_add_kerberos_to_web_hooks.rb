class AddKerberosToWebHooks < ActiveRecord::Migration
  include Gitlab::Database::MigrationHelpers

  # Set this constant to true if this migration requires downtime.
  DOWNTIME = false

  disable_ddl_transaction!

  def up
    add_column_with_default(:web_hooks, :kerberos, :boolean, default: false)
  end

  def down
    remove_column(:web_hooks, :kerberos)
  end
end
