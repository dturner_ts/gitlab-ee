class WebHookService
  class InternalErrorResponse
    attr_reader :body, :headers, :code

    def initialize
      @headers = {}
      @body = ''
      @code = 'internal error'
    end
  end

  attr_accessor :hook, :data, :hook_name

  def initialize(hook, data, hook_name)
    @hook = hook
    @data = data
    @hook_name = hook_name
  end

  def execute
    start_time = Time.now

    request = if parsed_url.userinfo.blank?
                make_request(hook.url)
              else
                make_request_with_auth
              end

    if hook.kerberos
      request.auth.gssnegotiate
    end

    response = JsonHTTPWrapper.post(request)

    log_execution(
      trigger: hook_name,
      url: hook.url,
      request_data: data,
      response: response,
      execution_duration: Time.now - start_time
    )

    [response.code, response.body]
  rescue SocketError, OpenSSL::SSL::SSLError, Errno::ECONNRESET, Errno::ECONNREFUSED, Net::OpenTimeout => e
    log_execution(
      trigger: hook_name,
      url: hook.url,
      request_data: data,
      response: InternalErrorResponse.new,
      execution_duration: Time.now - start_time,
      error_message: e.to_s
    )

    Rails.logger.error("WebHook Error => #{e}")

    [nil, e.to_s]
  end

  def async_execute
    Sidekiq::Client.enqueue(WebHookWorker, hook.id, data, hook_name)
  end

  private

  def parsed_url
    @parsed_url ||= URI.parse(hook.url)
  end

  def make_request(url, basic_auth = false)
    request = HTTPI::Request.new(
      url: url,
      open_timeout: Gitlab.config.gitlab.webhook_timeout,
      read_timeout: Gitlab.config.gitlab.webhook_timeout,
      body: data.to_json,
      headers: build_headers(hook_name))

    request.auth.basic(basic_auth[:username], basic_auth[:password]) if basic_auth

    request.auth.ssl.verify_mode = hook.enable_ssl_verification ? :peer : :none
    request
  end

  def make_request_with_auth
    post_url = hook.url.gsub("#{parsed_url.userinfo}@", '')
    basic_auth = {
      username: parsed_url.user,
      password: parsed_url.password
    }
    make_request(post_url, basic_auth)
  end

  def log_execution(trigger:, url:, request_data:, response:, execution_duration:, error_message: nil)
    # logging for ServiceHook's is not available
    return if hook.is_a?(ServiceHook)

    WebHookLog.create(
      web_hook: hook,
      trigger: trigger,
      url: url,
      execution_duration: execution_duration,
      request_headers: build_headers(hook_name),
      request_data: request_data,
      response_headers: response.headers,
      response_body: response.body,
      response_status: response.code,
      internal_error_message: error_message
    )
  end

  def build_headers(hook_name)
    @headers ||= begin
      {
        'Content-Type' => 'application/json',
        'X-Gitlab-Event' => hook_name.singularize.titleize
      }.tap do |hash|
        hash['X-Gitlab-Token'] = hook.token if hook.token.present?
      end
    end
  end
end
