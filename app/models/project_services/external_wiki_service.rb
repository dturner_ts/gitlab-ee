class ExternalWikiService < Service
  prop_accessor :external_wiki_url

  validates :external_wiki_url, presence: true, url: true, if: :activated?

  def title
    'External Wiki'
  end

  def description
    'Replaces the link to the internal wiki with a link to an external wiki.'
  end

  def self.to_param
    'external_wiki'
  end

  def fields
    [
      { type: 'text', name: 'external_wiki_url', placeholder: 'The URL of the external Wiki', required: true }
    ]
  end

  def execute(_data)
    request = JsonHTTPWrapper::Request.new(url: properties['external_wiki_url'])
    request.auth.ssl.verify_mode = :peer
    @response = JsonHTTPWrapper.get(request) rescue nil
    unless response.success?
      nil
    end
  end

  def self.supported_events
    %w()
  end
end
